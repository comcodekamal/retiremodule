package com.Code.SCTPReceiver;

public enum EventType {
    START("START"),
    TRANSPORT_UNAVAILABLE("TRANSPORT_UNAVAILABLE"),
    TRANSPORT_AVAILABLE("TRANSPORT_AVAILABLE"),
    RX_MSG("RX_MSG"),
    TX_MSG("TX_MSG"),;

    private final String eventType;

    private EventType(String event) {
        this.eventType = event;
    }

    public static EventType getEvent(String eventType) {
        if (eventType == null) {
            return null;
        } else if (eventType.equalsIgnoreCase(START.getType())) {
            return START;
        } else if (eventType.equalsIgnoreCase(RX_MSG.getType())) {
            return RX_MSG;
        } else if (eventType.equalsIgnoreCase(TX_MSG.getType())) {
            return TX_MSG;
        } else if (eventType.equalsIgnoreCase(TRANSPORT_AVAILABLE.getType())) {
            return TRANSPORT_AVAILABLE;
        } else if (eventType.equalsIgnoreCase(TRANSPORT_UNAVAILABLE.getType())) {
            return TRANSPORT_UNAVAILABLE;
        }  else {
            return null;
        }
    }

    public String getType() {
        return eventType;
    }
}
