package com.Code.SCTPReceiver;

import com.comCode.ProtocolBuffer.Request;
import com.comCode.SctpMulti.Payload;

public class Event {
	protected EventType eventType;
	protected Request request;
	protected Payload payload;

	public Event(EventType eventType, Request request, Payload payload) {
		this.eventType = eventType;
		this.payload = payload;
		this.request = request;
	}

	public EventType getEventType() {
		return eventType;
	}

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public Payload getPayload() {
		return payload;
	}
}
