package com.Code.SCTPReceiver;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import com.Code.config.LocalConfig;
import com.Code.config.PeerConfig;
import com.comCode.Load.MessageRateSender;
import com.comCode.ProtocolBuffer.Message;
import com.comCode.ProtocolBuffer.Request;
import com.comCode.SctpMulti.Channel;
import com.comCode.SctpMulti.ChannelListener;
import com.comCode.SctpMulti.ChannelType;
import com.comCode.SctpMulti.Payload;
import com.comCode.SctpMulti.Server;
import com.comCode.Store.HandleRequestStore;
import com.google.protobuf.InvalidProtocolBufferException;

public class SchularChannelConnector implements ChannelListener {
	private static final Logger logger = Logger.getLogger(SchularChannelConnector.class);
	protected Random random = new Random();
	private PeerConfig peerConfig;
	private Channel channel;
	private Server server;
	private byte[] cerBuff = null;
	private boolean started = false;
	private ExecutorService msgDeliveryExecutor;
	boolean keepRunning = true;
	static BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));

	public SchularChannelConnector(PeerConfig peerConfig, LocalConfig localConfig) throws Exception {
		this.peerConfig = peerConfig;
		this.server = Server.Open(String.format("%s:%s", localConfig.getIpAddress(), peerConfig.getLocalPort()),
				localConfig.getIpAddress(), (short) peerConfig.getLocalPort(), null);

		this.channel = Channel.Open(peerConfig.getHostName(), localConfig.getIpAddress(), peerConfig.getLocalPort(),
				peerConfig.getRemoteIpAddress(), peerConfig.getRemotePort(), ChannelType.SERVER, this.cerBuff, null);
		this.server.addChannel(this.channel);
		this.channel.setChannelListener(this);
	}

	public String getName() {
		return this.peerConfig.getHostName();
	}

	public PeerConfig getPeerConfig() {
		return this.peerConfig;
	}

	public void onCommunicationUp(Channel association, int maxInboundStreams, int maxOutboundStreams) {
		try {
			ThreadContext.put("peer", getName());
			Event event = new Event(EventType.TRANSPORT_AVAILABLE,null, null);
			InvokeFSM(event);
		} catch (Exception e) {
			logger.error("failed to handle due to Exception=" + e);
		}
	}

	public void onCommunicationShutdown(Channel association) {
		try {
			ThreadContext.put("peer", getName());
			Event event = new Event(EventType.TRANSPORT_UNAVAILABLE,null, null);
			InvokeFSM(event);
		} catch (Exception e) {
			logger.error("failed to handle shut down indication in state", e);
		}
	}

	public void onCommunicationLost(Channel association) {
		try {
			ThreadContext.put("peer", getName());
			Event event = new Event(EventType.TRANSPORT_UNAVAILABLE, null,null);
			InvokeFSM(event);

		} catch (Exception e) {
			logger.error("failed to handle comm lost indication in ", e);
		}
	}

	public void onCommunicationRestart(Channel association) {

	}

	public void onPayload(Channel association, Payload payload) {
		try {
			ThreadContext.put("peer", getName());
			Event event = new Event(EventType.RX_MSG,null, payload);
			InvokeFSM(event);
		} catch (Exception e) {
			logger.error("failed to handle incoming data from Http Server in state", e);
		}
	}

	public void inValidStreamId(Payload payload) {

	}

	public void Send(Request request) throws Exception {
		logger.info("send request for MessageId[%d]:: ");
		Event event = new Event(EventType.TX_MSG,request, null);
		InvokeFSM(event);
	}

	public void start() {
		ThreadContext.put("peer", getName());
		if (started) {
			logger.error(String.format("peer [%s] already started", this.peerConfig.getHostName()));
			ThreadContext.getContext().remove("peer");
			try {
				throw new Exception(String.format("peer [%s] already started", this.peerConfig.getHostName()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		this.msgDeliveryExecutor = Executors.newFixedThreadPool(1);

		Event event = new Event(EventType.START,null, null);
		InvokeFSM(event);

		if (logger.isInfoEnabled()) {
			logger.info(String.format("started peer [%s]", this.peerConfig.getHostName()));
		}

		ThreadContext.getContext().remove("peer");
	}

	public void InvokeFSM(Event event) {

		if (this.msgDeliveryExecutor != null) {
			EventDeliveryHandler eventDeliveryHandler = new EventDeliveryHandler(event, this.peerConfig.getHostName());
			this.msgDeliveryExecutor.execute(eventDeliveryHandler);
		} else {
			logger.error(String.format("failed to post event %s to processor thread for peer %s",
					event.eventType.getType(), this.peerConfig.getHostName()));
		}

	}

	private void invokeFSM(Event event) {
		switch (event.getEventType()) {

		case START: {
			try {
				channel.start();
				if (this.started) {
					logger.error(String.format("Peer already started"));
					throw new Exception(String.format("peer [%s] already started", this.peerConfig.getHostName()));
				}

				if ((this.server != null) && (!this.server.isStarted())) {
					this.server.start();
				}
				this.channel.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (logger.isInfoEnabled()) {
				logger.info("----started-----");
			}

		}
			break;
		case TRANSPORT_UNAVAILABLE: {
			try {
				if (logger.isInfoEnabled()) {
					logger.info("tranport unavailable ,not ready to communicate ");
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
			break;
		case TRANSPORT_AVAILABLE: {

			try {
				if (logger.isInfoEnabled()) {
					logger.info("Connection accept, ready to communicate ");
				}
                
				

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
			break;
		case RX_MSG: {

			handleIncomingMessage(event.getPayload());
		}
			break;
		case TX_MSG: {
			try {

				final Message message = Message.newBuilder().setRequest(event.getRequest()).build();
				final byte[] binaryAlbum = message.toByteArray();
				Payload payload = new Payload(binaryAlbum.length, binaryAlbum, true, true, 46, 0, null,
						channel.getAssociation());
				this.channel.send(payload);
				if (logger.isInfoEnabled()) {
					logger.info("Request Send fro Id "+event.getRequest().getMessageId());
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Failed to recevice request due to Exception " + e);
			}
		}
			break;

		default: {
			logger.error("unknown event type=" + event.getEventType() + ", ignoring");
		}
		}
	}

	private void handleIncomingMessage(Payload payload) {

		if (payload != null) {

			try {
			
				logger.info("payload   "+payload.toString());
				if((payload.getData().length > 1)) {
					Request request = instantiateMessageFromBinary(payload.getData());
					logger.info("Message Recevied from request id  "+request.getMessageId());
					HandleRequestStore.getInstance().Remove(request.getMessageId());
				}else {
					MessageRateSender smscRateSender = new MessageRateSender();
					smscRateSender.sendAtRate();
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				channel.close();
				logger.error("failed to handle incoming message from http server over SCTP connection " + e);
			}

		} else {
			channel.close();
			return;
		}
	}

	private class EventDeliveryHandler implements Runnable {

		private Event event;
		private String peer;

		public EventDeliveryHandler(Event event, String peer) {
			this.event = event;
			this.peer = peer;
		}

		public void run() {
			ThreadContext.put("Peer-FSM", peer);
			try {
				if (logger.isDebugEnabled()) {
					logger.debug(String.format("got event type=%s on peer=%s in processor thread",
							event.eventType.getType(), peer));
				}
				synchronized (this) {
					invokeFSM(this.event);
				}
			} catch (Exception e) {
				logger.error(
						String.format("failed to handle event type=%s on peer=%s", event.eventType.getType(), peer), e);

			} finally {
				ThreadContext.getContext().remove("Peer-FSM");
			}
		}

	}
	
	
	public Request instantiateMessageFromBinary(final byte[] binaryAlbum) {

		Request request = null;

		try {
			final Message message = Message.parseFrom(binaryAlbum);
			request = message.getRequest();
		} catch (InvalidProtocolBufferException ipbe) {

			logger.error("ERROR: Unable to instantiate Message instance from provided binary data - " + ipbe);

		}

		return request;

	}
	
	

}
