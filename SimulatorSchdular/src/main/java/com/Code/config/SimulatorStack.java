package com.Code.config;

import org.apache.log4j.Logger;

import com.Code.SCTPReceiver.SchularChannelConnector;
import com.Code.config.LocalConfig;
import com.Code.config.PeerConfig;
import com.comCode.Store.HandleRequestStore;

public class SimulatorStack {

	private static Logger logger = Logger.getLogger(SimulatorStack.class);
	private static LocalConfig localConfig = null;
	private static SchularChannelConnector schularChannelConnector;

	public SimulatorStack() {
		HandleRequestStore.Init();
	}

	public static LocalConfig getLocalConfig() {
		return localConfig;
	}

	public static void setLocalConfig(LocalConfig localConfig) {
		SimulatorStack.localConfig = localConfig;
	}

	public static SchularChannelConnector getSchularChannelConnector() {
		return schularChannelConnector;
	}

	public static void setSchularChannelConnector(SchularChannelConnector schularChannelConnector) {
		SimulatorStack.schularChannelConnector = schularChannelConnector;
	}

	public void Start() {
		try {
			PeerConfig peerConfig = StackProperties.GetInstance().getPeerConfig();
			SchularChannelConnector schularChannelConnector = new SchularChannelConnector(peerConfig, localConfig);
			setSchularChannelConnector(schularChannelConnector);
			schularChannelConnector.start();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("failed to insitalize Sctp Connection" +e);
		}
	}

}
