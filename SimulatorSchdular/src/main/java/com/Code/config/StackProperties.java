package com.Code.config;

import java.io.FileNotFoundException;
import java.io.FileReader;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

public class StackProperties {

	private static Logger logger = Logger.getLogger(StackProperties.class);
	private static StackProperties instance;
	public static final String CONFIG_FILE = "iniFile";
	private LocalConfig localConfig;
	private PeerConfig peerConfig;
	private int totalNoOfMsg;
	private int ratePerSecond;
	private int liftTime;
	private int interval;
	private String msgTitle;

	public static StackProperties GetInstance() {
		return instance;
	}

	public static void Init() throws FileNotFoundException {
		String inifilename = System.getProperty(CONFIG_FILE);

		if (inifilename == null) {
			logger.error("Missing config file property: " + CONFIG_FILE);
			throw new RuntimeException("Missing config file property: " + CONFIG_FILE);
		}

		Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
		JsonReader reader = new JsonReader(new FileReader(inifilename));
		instance = gson.fromJson(reader, StackProperties.class);

		String jsonString = gson.toJson(instance, StackProperties.class);
		if (logger.isInfoEnabled()) {
			logger.info("----------------Loaded Configuration----------------");
			logger.info(jsonString);
			logger.info("----------------------------------------------------");
		}
	}

	public LocalConfig getLocalConfig() {
		return localConfig;
	}

	public void setLocalConfig(LocalConfig localConfig) {
		this.localConfig = localConfig;
	}

	public PeerConfig getPeerConfig() {
		return peerConfig;
	}

	public void setPeerConfig(PeerConfig peerConfig) {
		this.peerConfig = peerConfig;
	}

	public int getTotalNoOfMsg() {
		return totalNoOfMsg;
	}

	public void setTotalNoOfMsg(int totalNoOfMsg) {
		this.totalNoOfMsg = totalNoOfMsg;
	}

	public int getRatePerSecond() {
		return ratePerSecond;
	}

	public void setRatePerSecond(int ratePerSecond) {
		this.ratePerSecond = ratePerSecond;
	}

	public int getLiftTime() {
		return liftTime;
	}

	public void setLiftTime(int liftTime) {
		this.liftTime = liftTime;
	}

	public int getInterval() {
		return interval;
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public String getMsgTitle() {
		return msgTitle;
	}

	public void setMsgTitle(String msgTitle) {
		this.msgTitle = msgTitle;
	}

}
