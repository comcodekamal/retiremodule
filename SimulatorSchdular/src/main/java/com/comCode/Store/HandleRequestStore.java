package com.comCode.Store;

import java.util.concurrent.ConcurrentHashMap;
import org.apache.log4j.Logger;
import com.comCode.ProtocolBuffer.Request;

public class HandleRequestStore {
	private static Logger logger = Logger.getLogger(HandleRequestStore.class);
	protected static HandleRequestStore instance;

	protected ConcurrentHashMap<Integer, Request> handleRequsts;

	protected HandleRequestStore() {
		handleRequsts = new ConcurrentHashMap<Integer, Request>();
	}

	public static void Init() {
		HandleRequestStore.instance = new HandleRequestStore();
	}

	public static HandleRequestStore getInstance() {
		return instance;
	}

	public Request Get(int messageId) {
		return handleRequsts.get(messageId);
	}

	public void Add(Request request) {
		logger.info("Handle Request instance added in memory ");
		this.handleRequsts.put(request.getMessageId(), request);
	}

	public ConcurrentHashMap<Integer, Request> getRequest() {
		return handleRequsts;
	}

	public Request Remove(int messageId) {
		logger.info("Handle Request instance removing from memory ");
		return handleRequsts.remove(messageId);
	}

}
