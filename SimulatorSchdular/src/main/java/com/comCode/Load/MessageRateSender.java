package com.comCode.Load;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import com.Code.config.StackProperties;

public class MessageRateSender {

	private ScheduledThreadPoolExecutor sendScheduler;
	private int count = 0;
	private int ratePerSecond = 0;
	private int totalDeliver = 0;

	public MessageRateSender() {
		this.ratePerSecond = StackProperties.GetInstance().getRatePerSecond();
		this.totalDeliver = StackProperties.GetInstance().getTotalNoOfMsg();
	}

	public void sendAtRate() throws Exception {
		startWithRate(ratePerSecond);
	}

	public void stop() {
		resetSendScheduler();
	}

	private void startWithRate(final int ratePerSecond) {

		if (ratePerSecond <= 0) {
			System.out.println("Invalid rate");
			return;
		}

		resetSendScheduler();
		LoadSend runnable = new LoadSend() {

			public void run() {
				try {
					sendRequest();
					count++;
					if (count >= totalDeliver) {
						stop();
					}
				} catch (Exception ex) {
					System.out.println("Failed to send message" + ex);
				}

			}
		};

		long delay = 1000000 / ratePerSecond;
		sendScheduler.scheduleAtFixedRate(runnable, 0, delay, TimeUnit.MICROSECONDS);

	}

	private void resetSendScheduler() {
		if (sendScheduler != null && !sendScheduler.isTerminating()) {
			// Cancel scheduled but not started task, and avoid new ones
			sendScheduler.shutdown();

			// Wait for the running tasks
			try {
				sendScheduler.awaitTermination(5, TimeUnit.SECONDS);
			} catch (InterruptedException ex) {
				System.out.println("Interupt during awating termination" + ex);
			}

			// Interrupt the threads and shutdown the scheduler
			sendScheduler.shutdownNow();
		}
		sendScheduler = new ScheduledThreadPoolExecutor(1);
	}

	
	
	
}