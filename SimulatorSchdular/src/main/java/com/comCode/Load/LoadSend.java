package com.comCode.Load;

import org.apache.log4j.Logger;
import com.Code.config.SimulatorStack;
import com.Code.config.StackProperties;
import com.comCode.ProtocolBuffer.Request;
import com.comCode.Store.HandleRequestStore;
import com.google.protobuf.ByteString;

public class LoadSend implements Runnable {
	static Logger logger = Logger.getLogger(LoadSend.class.getName());
    static int messageId=0; 
	
	public void run() {

	}

	public void sendRequest() {
		try {
			Request request=generateRequest();
			HandleRequestStore.getInstance().Add(request);
			SimulatorStack.getSchularChannelConnector().Send(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public com.comCode.ProtocolBuffer.Payload generatePayload() {
		com.comCode.ProtocolBuffer.Payload.Builder builder = com.comCode.ProtocolBuffer.Payload.newBuilder();
		String str="comcode technology "+String.valueOf(messageId);
		ByteString byteString = ByteString.copyFrom(str.getBytes());
		com.comCode.ProtocolBuffer.Payload payload = builder.setStrem(byteString).build();
		return payload;
	}

	public Request generateRequest() {
		com.comCode.ProtocolBuffer.Request.Builder builder = com.comCode.ProtocolBuffer.Request.newBuilder();
		Request request = builder.setTitle(StackProperties.GetInstance().getMsgTitle()).setInterval(StackProperties.GetInstance().getInterval()).setLifeTime(StackProperties.GetInstance().getLiftTime()).setMessageId(messageId++)
				.setPayload(generatePayload()).build();
		return request;
	}
	
}