package com.comCode;

import java.io.FileNotFoundException;
import org.apache.log4j.Logger;

import com.Code.config.SimulatorStack;
import com.Code.config.StackProperties;

public class MainApp {

	private static Logger logger = Logger.getLogger(MainApp.class);
	public static SimulatorStack simulatorStack = null;
	public static void main(String[] args) {
		if (logger.isInfoEnabled()) {
			logger.warn("------------------------------------------------------------");
			logger.warn("--------------------Starting Schdular Server----------------");
			logger.warn("------------------------------------------------------------");
		}
		try {
			StackProperties.Init();
		} catch (FileNotFoundException e) {
			logger.error("failed to load Schdular configuration", e);
			return;
		}

		try {
			simulatorStack = new SimulatorStack();
		} catch (Exception e) {
			logger.error("failed to initialize stack", e);
			return;
		}

		if (StackProperties.GetInstance().getLocalConfig() != null) {
			SimulatorStack.setLocalConfig(StackProperties.GetInstance().getLocalConfig());
		}

		try {
			simulatorStack.Start();
		} catch (Exception e) {
			logger.error("failed to start schdular stack", e);
			return;
		}
		logger.info("schdular stack started successfully");
	}
}
