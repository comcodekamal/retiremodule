// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: src/main/java/proto/message.proto

package com.comCode.ProtocolBuffer;

public final class MessageRequest {
	private MessageRequest() {
	}

	public static void registerAllExtensions(com.google.protobuf.ExtensionRegistryLite registry) {
	}

	public static void registerAllExtensions(com.google.protobuf.ExtensionRegistry registry) {
		registerAllExtensions((com.google.protobuf.ExtensionRegistryLite) registry);
	}

	static final com.google.protobuf.Descriptors.Descriptor internal_static_Request_descriptor;
	static final com.google.protobuf.GeneratedMessageV3.FieldAccessorTable internal_static_Request_fieldAccessorTable;
	static final com.google.protobuf.Descriptors.Descriptor internal_static_Payload_descriptor;
	static final com.google.protobuf.GeneratedMessageV3.FieldAccessorTable internal_static_Payload_fieldAccessorTable;
	static final com.google.protobuf.Descriptors.Descriptor internal_static_Message_descriptor;
	static final com.google.protobuf.GeneratedMessageV3.FieldAccessorTable internal_static_Message_fieldAccessorTable;

	public static com.google.protobuf.Descriptors.FileDescriptor getDescriptor() {
		return descriptor;
	}

	private static com.google.protobuf.Descriptors.FileDescriptor descriptor;
	static {
		java.lang.String[] descriptorData = { "\n!src/main/java/proto/message.proto\"j\n\007R"
				+ "equest\022\r\n\005title\030\001 \002(\t\022\020\n\010lifeTime\030\002 \001(\005\022"
				+ "\020\n\010interval\030\003 \001(\005\022\031\n\007payload\030\004 \002(\0132\010.Pay"
				+ "load\022\021\n\tmessageId\030\005 \001(\005\"\030\n\007Payload\022\r\n\005st"
				+ "rem\030\001 \002(\014\"$\n\007Message\022\031\n\007request\030\001 \002(\0132\010."
				+ "RequestB.\n\032com.comCode.ProtocolBufferB\016M" + "essageRequestP\001" };
		descriptor = com.google.protobuf.Descriptors.FileDescriptor.internalBuildGeneratedFileFrom(descriptorData,
				new com.google.protobuf.Descriptors.FileDescriptor[] {});
		internal_static_Request_descriptor = getDescriptor().getMessageTypes().get(0);
		internal_static_Request_fieldAccessorTable = new com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
				internal_static_Request_descriptor,
				new java.lang.String[] { "Title", "LifeTime", "Interval", "Payload", "MessageId", });
		internal_static_Payload_descriptor = getDescriptor().getMessageTypes().get(1);
		internal_static_Payload_fieldAccessorTable = new com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
				internal_static_Payload_descriptor, new java.lang.String[] { "Strem", });
		internal_static_Message_descriptor = getDescriptor().getMessageTypes().get(2);
		internal_static_Message_fieldAccessorTable = new com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
				internal_static_Message_descriptor, new java.lang.String[] { "Request", });
	}

	// @@protoc_insertion_point(outer_class_scope)
}
