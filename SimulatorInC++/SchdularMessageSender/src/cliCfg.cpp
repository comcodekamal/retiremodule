#include <stdlib.h>
#include"cliCfg.h"


cliCfg::cliCfg()
{
}

string cliCfg::getSrcHost()
{
    return m_srcHost;
}

string cliCfg::getDstHost()
{
    return m_dstHost;
}

void cliCfg::initCfg()
{

   if(getenv("srcHost") != NULL)
    {
        m_srcHost = getenv("srcHost");
    }
    else
    {
        m_srcHost = "192.168.1.155";
    }

    printf(" Source host = %s\n",m_srcHost.c_str());

   if(getenv("dstHost") != NULL)
    {
        m_dstHost = getenv("dstHost");
    }
    else
    {
        m_dstHost = "192.168.1.152";
    }
    printf(" Destination host = %s\n",m_dstHost.c_str());

    

   if(getenv("dstPort") != NULL)
    {
        m_dstPort = atoi(getenv("dstPort"));
    }
    else
    {
        m_dstPort = 3868;
    }
    printf(" Destination port = %d\n",m_dstPort);


   if(getenv("srcPort")!= NULL)
    {
        m_srcPort = atoi(getenv("srcPort"));
    }
    else
    {
        m_srcPort = 3868;
    }
    printf(" Source port = %d\n",m_srcPort);

    
   if(getenv("msgDWR")!= NULL)
    {
        m_mDWR = getenv("msgDWR");
    }
    else
    {
        m_mDWR = "NOMessage";
    }
    printf(" message DWR = %s\n",m_mDWR.c_str());

}
