#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>
#include <iostream>
#include <ctime>
#include<iostream>
#include<string> 
#include"cliCfg.h"
#define MAX_BUFFER 1024
#define MY_PORT_NUM 3868 /* This can be changed to suit the need and should be same in server and client */
#include "RequestSm.h"
#include <google/protobuf/io/coded_stream.h>
using namespace std;
unsigned char* hexstr_to_char(const char* hexstr, int *outlen);
void send_request(int connSock,unsigned char *msgBytes, int datalen);
int main (int argc, char* argv[])
{
    GOOGLE_PROTOBUF_VERIFY_VERSION;
    int connSock, ret;
    struct sockaddr_in servaddr, adr_client;
    char buffer[MAX_BUFFER + 1];
    int datalen = 0;
    cliCfg *objCli = new cliCfg();
    objCli->initCfg();
    /*Get the input from user*/
    /* Clear the newline or carriage return from the end*/
    buffer[strcspn(buffer, "\r\n")] = 0;
    /* Sample input */

    datalen = strlen(buffer);

    connSock = socket (AF_INET, SOCK_STREAM, IPPROTO_SCTP);

    if (connSock == -1)
    {
        printf("Socket creation failed\n");
        perror("socket()");
        exit(1);
    }

    memset(&adr_client,0,sizeof(adr_client));

    adr_client.sin_family = AF_INET;
    adr_client.sin_port = htons(objCli->getSrcPort());
    adr_client.sin_addr.s_addr = inet_addr (objCli->getSrcHost().c_str());

    ret = bind(connSock,(struct sockaddr *)&adr_client,
            sizeof(adr_client));  

    if (ret == -1)
    {
        printf("Bind failed\n");
        perror("bind()");
        close(connSock);
        exit(1);
    }

    bzero ((void *) &servaddr, sizeof (servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons (objCli->getDstPort());
    servaddr.sin_addr.s_addr = inet_addr (objCli->getDstHost().c_str());

    ret = connect (connSock, (struct sockaddr *) &servaddr, sizeof (servaddr));

    if (ret == -1)
    {
        printf("Connection failed\n");
        perror("connect()");
        close(connSock);
        exit(1);
    }
    int msgOption;
    unsigned char *msgBytes=NULL;
   while(1)
    {
    printf("Enter data to send:(1) DWR ");
    scanf("%d",&msgOption);
    switch(msgOption)
    {
        case 1: 
        {
            int count=0;
            for(int i=1; i<=10000;i++){
            
                  if(count == 500){
                     count = 0;  
                     sleep(1.0);
                  }
                  string data;
                  ProtoBuf::Message message;
                  ProtoBuf::Request *request = new ProtoBuf::Request();
                  request->set_messageid(i);
                  cout << "MessageID: " << i << endl << endl;
                  request->set_lifetime(2000);
                  request->set_interval(2000);
                  request->set_title("START");
                  ProtoBuf::Payload *payload = new ProtoBuf::Payload();
                  payload->set_strem("comcode technologies");
                  
                  request->set_allocated_payload(payload);
                  message.set_allocated_request(request);
                  
                  message.SerializeToString(&data);
                  cout << "SerializeToString is Done" << endl << endl;
                  
                  unsigned char trap[256];
                  std::copy( data.begin(), data.end(), trap );
                  trap[data.length()] = 0;
                  std::cout << trap << std::endl;
                  datalen=data.length();
                  msgBytes=trap; 
                  send_request(connSock,msgBytes,datalen);
                  count++;
                
            }
            
        }
        break;
    }

}
    close (connSock);

    return 0;
}


unsigned char* hexstr_to_char(const char* hexstr, int *outlen)
{
    size_t len = strlen(hexstr);
    size_t i = 0;
    size_t j = 0;
    if(len % 2 != 0)
        return NULL;
    size_t final_len = len / 2;
    unsigned char* chrs = (unsigned char*)malloc((final_len+1) * sizeof(*chrs));
    for (i=0, j=0; j<final_len; i+=2, j++)
    {
        chrs[j] = (hexstr[i] % 32 + 9) % 25 * 16 + (hexstr[i+1] % 32 + 9) % 25;
        printf(" 0x%x ", chrs[j]);
    }
    *outlen = final_len;
    chrs[final_len] = '\0';
    return chrs;
}

void send_request(int connSock,unsigned char* msgBytes,int datalen){

    int ret = sctp_sendmsg (connSock, (void *) msgBytes, (size_t) datalen,
            NULL, 0, 0, 0, 0, 0, 0);
    if(ret == -1 )
    {
        printf("Error in sctp_sendmsg\n");
        perror("sctp_sendmsg()");
    }
    else
        printf("Successfully sent %d bytes data to server\n", ret);

}

void sleep(float seconds){
    clock_t startClock = clock();
    float secondsAhead = seconds * CLOCKS_PER_SEC;
    // do nothing until the elapsed time has passed.
    while(clock() < startClock+secondsAhead);
    return;
}
