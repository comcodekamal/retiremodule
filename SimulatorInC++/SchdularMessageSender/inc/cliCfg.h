#include <iostream>
#include <string>
using namespace std;


class cliCfg   
{
    string m_srcHost;
    string m_dstHost;
    string m_mDWR;
    short m_srcPort;
    short m_dstPort;

    public:
    cliCfg();

    string getSrcHost();

    string getDstHost();

    short getSrcPort()
    {
        return m_srcPort;
    }
    short getDstPort()
    {
        return m_dstPort;
    }

    const char * getMsgDwr()
    {
          return  m_mDWR.c_str();
    }


    void initCfg();
       
};
