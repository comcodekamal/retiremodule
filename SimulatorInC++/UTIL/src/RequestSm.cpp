#include "RequestSm.h"
#include <google/protobuf/io/coded_stream.h>

Request :: Request()
{
    payload = NULL;
    title = "" ;
    messageId = 0;
    lifeTimer = 0;
    intervalTimer = 0;
}

string Request :: GetTitle()
{
    return title;
}

void Request :: SetTitle(string title)
{
    this->title = title;
}


U8 Request :: GetMessageId()
{
    return messageId;
}

void Request :: SetMessageId(U8 messageId)
{
    this->messageId = messageId;
}

U8 Request :: GetLifeTimer()
{
    return lifeTimer;
}

void Request :: SetLifeTimer(U8 lifeTimer)
{
    this->lifeTimer = lifeTimer;
}

U8 Request :: GetIntervalTimer()
{
    return intervalTimer;
}

void Request :: SetIntervalTimer(U8 intervalTimer)
{
    this->intervalTimer = intervalTimer;
}

void Request :: SetPayload(Payload &payload)
{
    this->payload = new Payload(payload);
}

Payload * Request :: GetPayload()
{
    return this->payload;
}


bool Request :: Encode(U8 *buffer, U16 *len)
{
    bool result = false;
    string msg = "";
    ProtoBuf::Message message;
    
    ProtoBuf::Request *request = new ProtoBuf::Request();
    request->set_messageid(messageId);
    request->set_lifetime(lifeTimer);
    request->set_interval(intervalTimer);
    message.set_allocated_request(request);
   
    ProtoBuf::Payload *payload = new ProtoBuf::Payload();
    payload->set_strem(this->payload->GetStrem());
   
    int writtenBytes = message.ByteSizeLong();
    result = (message.SerializeToArray(buffer, *len));
    if(!result)
    {
        
    }
    *len = writtenBytes;
    return result;
}

int Request :: Decode(ProtoBuf::Message *message)
{
    ProtoBuf::Request request = message->request();

    this->messageId = request.messageid();
    this->title = request.title();
    
    if(request.has_lifetime())
    {
        this->lifeTimer = request.lifetime();
    }
	  if(request.has_interval())
    {
        this->intervalTimer=request.interval();
    }
    
    ProtoBuf::Payload payload = request.payload();
    this->payload->SetStrem(payload.strem());

    return 0;
}

