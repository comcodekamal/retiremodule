#ifndef __PAYLOAD__
#define __PAYLOAD__

#include "All_Types.h"

class Payload
{
    private:
        string strem;
       
    public:
        Payload(string strem);
        Payload();
        
        void SetStrem(string strem);
        string GetStrem();

};
#endif
