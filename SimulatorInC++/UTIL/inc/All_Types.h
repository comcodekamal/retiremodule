#ifndef __ALL_TYPES__
#define __ALL_TYPES__

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <stdarg.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/errno.h>
#include <errno.h>
#include <ctype.h>
#include <netdb.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netinet/sctp.h>
#include <netinet/sctp.h>
#include <sstream>
#include <string>
#include <map>
#include <unordered_map>
#include <list>
#include <fstream>
#include <algorithm>

using namespace std;

typedef uint8_t     U8;
typedef uint16_t    U16;
typedef uint32_t    U32;
typedef int32_t     S32;
typedef int16_t     S16;
typedef int8_t      S8;
typedef uint64_t    U64;


#endif
