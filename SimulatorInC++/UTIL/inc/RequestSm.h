#ifndef __REQUESTSM__
#define __REQUESTSM__

#include "All_Types.h"
#include "messages.pb.h"
#include "Payload.h"

class Request
{
    private:
    
        Payload *payload;
        U8 messageId;
        string title;
        U8 lifeTimer;
	    	U8 intervalTimer;
           
    public:

        Request();
        ~Request();

        U8 GetMessageId();
        void SetMessageId(U8 messageId); 
        
        string GetTitle();
        void SetTitle(string title);

        U8 GetLifeTimer();
        void SetLifeTimer(U8 lifeTimer);

        U8 GetIntervalTimer();
        void SetIntervalTimer(U8 intervalTimer);

        bool Encode(U8 *buffer, U16 *len);
        int Decode(ProtoBuf::Message *message);
        
        void SetPayload(Payload &payload);
        Payload *GetPayload();
};

#endif
