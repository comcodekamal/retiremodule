package com.Code;

import org.apache.log4j.Logger;
import com.Code.SCTPReceiver.SchularChannelRecevier;
import com.Code.config.LocalConfig;
import com.Code.config.PeerConfig;
import com.comCode.HibernateFactory.HibernateUtil;
import com.comCode.Store.HandleRequestStore;
import com.comCode.storage.api.RequestDBStore;
import com.comCode.storage.impl.PersistentRequestStoreImpl;

public class SchdularStack {

	private static Logger logger = Logger.getLogger(SchdularStack.class);
	private static LocalConfig localConfig = null;
	private static SchularChannelRecevier schularChannelRecevier;
	private static RequestDBStore requestDBStore;

	public SchdularStack() {
		try {
			HandleRequestStore.Init();
			if (SchdularProperties.GetInstance().getSchdularConfig().isDbSupport()) {
				requestDBStore = new PersistentRequestStoreImpl();
				HibernateUtil.Init();
			}
		} catch (Exception e) {
			logger.error(" ERROR: Unable to invoke init for hibernate & request store " + e);
		}

	}

	public static LocalConfig getLocalConfig() {
		return localConfig;
	}

	public static void setLocalConfig(LocalConfig localConfig) {
		SchdularStack.localConfig = localConfig;
	}

	public static SchularChannelRecevier getSchularChannelRecevier() {
		return schularChannelRecevier;
	}

	public static void setSchularChannelRecevier(SchularChannelRecevier sChannelRecevier) {
		schularChannelRecevier = sChannelRecevier;
	}

	public static RequestDBStore getRequestDBStore() {
		return requestDBStore;
	}

	public static void setRequestDBStore(RequestDBStore requestDBStore) {
		SchdularStack.requestDBStore = requestDBStore;
	}

	public void Start() {
		try {
			PeerConfig peerConfig = SchdularProperties.GetInstance().getPeerConfig();
			schularChannelRecevier = new SchularChannelRecevier(peerConfig, localConfig);
			schularChannelRecevier.start();
		} catch (Exception e) {
			logger.error(String.format("ERROR: Unable to initialize sctp connection ")+ e);
		}
	}

}
