package com.Code;

import com.Code.config.LocalConfig;
import com.Code.config.PeerConfig;
import com.Code.config.SchdularConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import org.apache.log4j.Logger;

public class SchdularProperties {

	private static Logger logger = Logger.getLogger(SchdularProperties.class);
	private static SchdularProperties instance;
	public static final String CONFIG_FILE = "iniFile";
	private SchdularConfig schdularConfig;
	private LocalConfig localConfig;
	private PeerConfig peerConfig;

	public static SchdularProperties GetInstance() {
		return instance;
	}

	public static void Init() throws FileNotFoundException {
		String inifilename = System.getProperty(CONFIG_FILE);

		if (inifilename == null) {
			logger.error("Missing config file property: " + CONFIG_FILE);
			throw new RuntimeException("Missing config file property: " + CONFIG_FILE);
		}

		Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
		JsonReader reader = new JsonReader(new FileReader(inifilename));
		instance = gson.fromJson(reader, SchdularProperties.class);

		String jsonString = gson.toJson(instance, SchdularProperties.class);
		if (logger.isInfoEnabled()) {
			logger.info("----------------Loaded Configuration----------------");
			logger.info(jsonString);
			logger.info("----------------------------------------------------");
		}
	}

	public SchdularConfig getSchdularConfig() {
		return schdularConfig;
	}

	public void setSchdularConfig(SchdularConfig schdularConfig) {
		this.schdularConfig = schdularConfig;
	}

	public LocalConfig getLocalConfig() {
		return localConfig;
	}

	public void setLocalConfig(LocalConfig localConfig) {
		this.localConfig = localConfig;
	}

	public PeerConfig getPeerConfig() {
		return peerConfig;
	}

	public void setPeerConfig(PeerConfig peerConfig) {
		this.peerConfig = peerConfig;
	}

}
