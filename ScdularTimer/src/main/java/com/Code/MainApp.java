package com.Code;

import java.io.FileNotFoundException;
import org.apache.log4j.Logger;

import com.Code.Handler.HandleRequst;

public class MainApp {

	private static Logger logger = Logger.getLogger(MainApp.class);
	public static SchdularStack schdularStack = null;

	public static void main(String[] args) {
		if (logger.isInfoEnabled()) {
			logger.warn("------------------------------------------------------------");
			logger.warn("--------------------Starting Schdular Server----------------");
			logger.warn("------------------------------------------------------------");
		}
		try {
			SchdularProperties.Init();
		} catch (FileNotFoundException e) {
			logger.error("ERROR: Unable to instantiate schdular configuration", e);
			return;
		}

		try {
			schdularStack = new SchdularStack();
		} catch (Exception e) {
			logger.error("ERROR: Unable to initialize stack", e);
			return;
		}

		if (SchdularProperties.GetInstance().getLocalConfig() != null) {
			SchdularStack.setLocalConfig(SchdularProperties.GetInstance().getLocalConfig());
		}

		try {
			try
			{
				logger.info(" wait 5 sec for loading all configuration initializing! ");
			    Thread.sleep(5000);
			}
			catch(InterruptedException ex)
			{
			    Thread.currentThread().interrupt();
			}
			schdularStack.Start();
			
		} catch (Exception e) {
			logger.error(" ERROR: Unable to start schdular stack ", e);
			return;
		}
		logger.info("schdular stack started successfully!");
	}
}
