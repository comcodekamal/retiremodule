package com.Code.Handler;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.apache.log4j.Logger;
import com.Code.SchdulerTimer;
import com.google.gson.annotations.Expose;

@Entity
@Table(name = "handleRequst")
public class HandleRequst {

	private static Logger logger = Logger.getLogger("testLogger");
	@Expose
	@Transient
	private SchdulerTimer schdulerTimer;

	@Expose
	@Column(name = "messageTitle")
	private String messageTitle;
	@Expose
	@Id
	@Column(name = "messageId")
	private int messageId;
	@Expose
	@Column(name = "lifeTime")
	private int lifeTime;
	@Expose
	@Column(name = "intervalTime")
	private int intervalTime;
	@Expose
	@Column(name = "startTime")
	private String startTime;
	@Expose
	@Column(name = "expireTime")
	private String expireTime;
	@Expose
	@Column(name = "maxRetire")
	private int maxRetire;
	@Expose
	@Column(name = "totalRetireDone")
	private int totalRetireDone;
	@Expose
	@Column(name = "actualLifeTime")
	private int actualLifeTime;
	@Expose
	@Column(name = "actualintervalTime")
	private int actualintervalTime;
	@Expose
	@Column(name = "lifeTimeExpire")
	private boolean lifeTimeExpire;
	@Expose
	@Column(name = "payloadData")
	private byte[] payloadData;

	public HandleRequst() {
		this.startTime = java.time.LocalDateTime.now().toString();
	}

	public int getMessageId() {
		return messageId;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public int getLifeTime() {
		return lifeTime;
	}

	public void setLifeTime(int lifeTime) {
		this.lifeTime = lifeTime;
	}

	public int getIntervalTime() {
		return intervalTime;
	}

	public void setIntervalTime(int intervalTime) {
		this.intervalTime = intervalTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(String expireTime) {
		this.expireTime = expireTime;
	}

	public byte[] getPayloadData() {
		return payloadData;
	}

	public void setPayloadData(byte[] payloadData) {
		this.payloadData = payloadData;
	}

	public String getMessageTitle() {
		return messageTitle;
	}

	public void setMessageTitle(String messageTitle) {
		this.messageTitle = messageTitle;
	}
	
	public int getMaxRetire() {
		return maxRetire;
	}

	public void setMaxRetire(int maxRetire) {
		this.maxRetire = maxRetire;
	}

	public int getTotalRetireDone() {
		return totalRetireDone;
	}

	public void setTotalRetireDone(int totalRetireDone) {
		this.totalRetireDone = totalRetireDone;
	}

	public int getActualLifeTime() {
		return actualLifeTime;
	}

	public void setActualLifeTime(int actualLifeTime) {
		this.actualLifeTime = actualLifeTime;
	}

	public int getActualintervalTime() {
		return actualintervalTime;
	}

	public void setActualintervalTime(int actualintervalTime) {
		this.actualintervalTime = actualintervalTime;
	}

	public boolean isLifeTimeExpire() {
		return lifeTimeExpire;
	}

	public void setLifeTimeExpire(boolean lifeTimeExpire) {
		this.lifeTimeExpire = lifeTimeExpire;
	}

	public void startTimer() {
		if (this.schdulerTimer == null) {
			this.schdulerTimer = SchdulerTimer.startTimer(this);
		} else {
			logger.debug("can't start timer for handleRequst=" + this.getMessageId());
		}
	}

	public void stopTimer() {
		this.totalRetireDone++;
		if (this.schdulerTimer != null) {
			this.schdulerTimer.cancel(true);
			this.schdulerTimer = null;
		}
	}

	public void assignValues() {
		this.maxRetire = this.lifeTime / this.intervalTime;
		this.totalRetireDone = 0;
		this.actualLifeTime = this.lifeTime;
		this.actualintervalTime = this.intervalTime;
		this.lifeTimeExpire = false;
	}

	@Override
	public String toString() {
		return String.format("[key=%d lifeTime=%d intervalTime=%d startTime=%s expireTime=%s", messageId,
				lifeTime, intervalTime, startTime, expireTime);
	}

	public void logPrint() {

		if (logger.isInfoEnabled()) {
			logger.info(String.format(
					"[meessageId=%d messageTitle=%s lifeTime=%d intervalTime=%d startTime=%s expireTime=%s maxRetire=%d totalRetireDone=%d",
					messageId, messageTitle, actualLifeTime, actualintervalTime, startTime, expireTime, maxRetire,
					totalRetireDone));
		}

	}
}
