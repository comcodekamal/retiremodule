package com.Code.SCTPReceiver;

import com.Code.Handler.HandleRequst;
import com.comCode.SctpMulti.Payload;

public class Event {
	protected EventType eventType;
	protected HandleRequst handleRequst;
	protected Payload payload;

	public Event(EventType eventType, HandleRequst handleRequst, Payload payload) {
		this.eventType = eventType;
		this.handleRequst = handleRequst;
		this.payload = payload;
	}

	public EventType getEventType() {
		return eventType;
	}

	public HandleRequst getHandleRequst() {
		return handleRequst;
	}

	public void setHandleRequst(HandleRequst handleRequst) {
		this.handleRequst = handleRequst;
	}

	public Payload getPayload() {
		return payload;
	}
}
