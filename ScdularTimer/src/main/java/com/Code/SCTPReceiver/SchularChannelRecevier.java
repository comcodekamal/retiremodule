package com.Code.SCTPReceiver;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import com.Code.SchdularProperties;
import com.Code.SchdularStack;
import com.Code.Handler.HandleRequst;
import com.Code.config.LocalConfig;
import com.Code.config.PeerConfig;
import com.comCode.ProtocolBuffer.Message;
import com.comCode.ProtocolBuffer.Request;
import com.comCode.SctpMulti.Channel;
import com.comCode.SctpMulti.ChannelListener;
import com.comCode.SctpMulti.ChannelType;
import com.comCode.SctpMulti.Payload;
import com.comCode.SctpMulti.Server;
import com.comCode.Store.HandleRequestStore;
import com.google.protobuf.ByteString;

public class SchularChannelRecevier implements ChannelListener {
	private static final Logger logger = Logger.getLogger(SchularChannelRecevier.class);
	private static final int MYTHREADS = 10;
	private PeerConfig peerConfig;
	private Channel channel;
	private Server server;
	private byte[] cerBuff = null;
	private boolean started = false;
	private ExecutorService msgDeliveryExecutor;
	private boolean dbSupport;

	public SchularChannelRecevier(PeerConfig peerConfig, LocalConfig localConfig) throws Exception {
		try {
			this.peerConfig = peerConfig;
			this.dbSupport = SchdularProperties.GetInstance().getSchdularConfig().isDbSupport();
			this.cerBuff = new byte[1];
			this.channel = Channel.Open(peerConfig.getHostName(), localConfig.getIpAddress(), peerConfig.getLocalPort(),
					peerConfig.getRemoteIpAddress(), peerConfig.getRemotePort(), ChannelType.CLIENT, this.cerBuff,
					null);
			this.channel.setChannelListener(this);
		} catch (Exception e) {
			logger.error(String.format("ERROR: Unable to create sctp cpnnection for peer[%s] " ,peerConfig.getHostName()) +e);
		}
	}

	public String getName() {
		return this.peerConfig.getHostName();
	}

	public PeerConfig getPeerConfig() {
		return this.peerConfig;
	}

	public void onCommunicationUp(Channel association, int maxInboundStreams, int maxOutboundStreams) {
		try {
			ThreadContext.put("peer", getName());
			Event event = new Event(EventType.TRANSPORT_AVAILABLE, null, null);
			InvokeFSM(event);
		} catch (Exception e) {
			logger.error(String.format("ERROR: Unable to handle COMM-UP for peer[%s] " ,peerConfig.getHostName()) +e);
		}
	}

	public void onCommunicationShutdown(Channel association) {
		try {
			ThreadContext.put("peer", getName());
			Event event = new Event(EventType.TRANSPORT_UNAVAILABLE, null, null);
			InvokeFSM(event);
		} catch (Exception e) {
			logger.error(String.format("ERROR: Unable to handle SHUT-DOWN for peer[%s] " ,peerConfig.getHostName()) +e);
		}
	}

	public void onCommunicationLost(Channel association) {
		try {
			ThreadContext.put("peer", getName());
			Event event = new Event(EventType.TRANSPORT_UNAVAILABLE, null, null);
			InvokeFSM(event);
		} catch (Exception e) {
			logger.error(String.format("ERROR: Unable to handle COMUP-LOST for peer[%s] " ,peerConfig.getHostName()) +e);
		}
	}

	public void onCommunicationRestart(Channel association) {

	}

	public void onPayload(Channel association, Payload payload) {
		try {
			ThreadContext.put("peer", getName());
			Event event = new Event(EventType.RX_MSG, null, payload);
			InvokeFSM(event);
		} catch (Exception e) {
			logger.error(String.format("ERROR: Unable to handle payload data from sctp connection for peer[%s] " ,peerConfig.getHostName()) +e);
		}
	}

	public void inValidStreamId(Payload payload) {

	}

	public void Send(HandleRequst handleRequst) throws Exception {
		try {
	   	   Event event = new Event(EventType.TX_MSG, handleRequst, null);
		   invokeFSM(event);
		}catch (Exception e) {
			logger.error(String.format("ERROR: Unable to send response data for messageId[%d] " ,handleRequst.getMessageId()) +e);
		}
	}

	public void start() {
		ThreadContext.put("peer", getName());
		if (started) {
			logger.error(String.format(" ERROR: peer [%s] already started", this.peerConfig.getHostName()));
			ThreadContext.getContext().remove("peer");
			try {
				throw new Exception(String.format("ERROR: peer [%s] already started", this.peerConfig.getHostName()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		this.msgDeliveryExecutor = Executors.newFixedThreadPool(MYTHREADS);

		Event event = new Event(EventType.START, null, null);
		InvokeFSM(event);

		if (logger.isInfoEnabled()) {
			logger.info(String.format("started peer [%s]", this.peerConfig.getHostName()));
		}

		ThreadContext.getContext().remove("peer");
	}

	public void InvokeFSM(Event event) {

		if (this.msgDeliveryExecutor != null) {
			EventDeliveryHandler eventDeliveryHandler = new EventDeliveryHandler(event, this.peerConfig.getHostName());
			this.msgDeliveryExecutor.execute(eventDeliveryHandler);
		} else {
			logger.error(String.format("ERROR: Unable to post event %s to processor thread for peer %s " ,event.eventType.getType(), this.peerConfig.getHostName()));
			
		}

	}

	public void invokeFSM(Event event) {
		
		switch (event.getEventType()) {

		case START: {
			try {
				channel.start();
				if (this.started) {
					logger.error(String.format("Peer already started"));
					throw new Exception(String.format("peer [%s] already started", this.peerConfig.getHostName()));
				}

				if ((this.server != null) && (!this.server.isStarted())) {
					this.server.start();
				}
				this.channel.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (logger.isInfoEnabled()) {
				logger.info("----started-----");
			}

		}
			break;
		case TRANSPORT_UNAVAILABLE: {
			try {
				if (logger.isInfoEnabled()) {
					logger.info("tranport unavailable ,not ready to communicate ");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
			break;
		case TRANSPORT_AVAILABLE: {

			try {
				if (logger.isInfoEnabled()) {
					logger.info("Connection accept, ready to communicate ");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
			break;
		case RX_MSG: {

			handleIncomingMessage(event.getPayload());
		}
			break;
		case TX_MSG: {
			try {
				HandleRequst handleRequst = event.getHandleRequst();
				logger.info("send response for messageId " + handleRequst.getMessageId() + " startTime "
						+ handleRequst.getStartTime() + " endTime " + handleRequst.getExpireTime());
				final Request request = generateRequest(handleRequst);
				final Message message = Message.newBuilder().setRequest(request).build();
				final byte[] binaryAlbum = message.toByteArray();
				Payload payload = new Payload(binaryAlbum.length, binaryAlbum, true, true, 46, 0, null,
						channel.getAssociation());
				this.channel.send(payload);
				
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Failed to recevice request due to Exception " + e);
			}
		}
			break;
		case INTERVAL_TIMER_EXPIRY: {
			try {
				HandleRequst handleRequst = event.getHandleRequst();
				handleRequst.stopTimer();
				if (handleRequst.getLifeTime() == 0) {
					
					handleRequst.setLifeTimeExpire(true);
					HandleRequestStore.getInstance().Remove(handleRequst.getMessageId());
					dbOperationHandler("DELETE",handleRequst);
					handleRequst.logPrint();
					Send(handleRequst);
			
				} else {
					if (handleRequst.getLifeTime() < handleRequst.getIntervalTime()) {
						handleRequst.setIntervalTime(handleRequst.getLifeTime());
						handleRequst.setLifeTime(0);
						dbOperationHandler("UPDATE",handleRequst);
						handleRequst.startTimer();
					} else {
						handleRequst.setLifeTime(handleRequst.getLifeTime() - handleRequst.getIntervalTime());
						dbOperationHandler("UPDATE",handleRequst);
						handleRequst.startTimer();
					}
				}

			} catch (Exception e) {
				logger.error("Interval timer expire " + e);
			}
		}
			break;
	
		default: {
			logger.error("unknown event type=" + event.getEventType() + ", ignoring");
		}
		}
	}

	private void handleIncomingMessage(Payload payload) {

		if (payload != null) {

			try {
				logger.info(" Message Recevied from sctp connection ");
				Request request = instantiateMessageFromBinary(payload.getData());
				if (request.getTitle().equalsIgnoreCase("START")) {
					try {
						HandleRequst handleRequst = createHandler(request);
						handleRequst.setLifeTime(handleRequst.getLifeTime() - handleRequst.getIntervalTime());
						handleRequst.startTimer();
					} catch (Exception e) {
						logger.error("failed to start new message timer " + e);
					}
				} else if (request.getTitle().equalsIgnoreCase("STOP")) {
					if (HandleRequestStore.getInstance().Get(request.getMessageId()) != null) {
						HandleRequst handleRequst = HandleRequestStore.getInstance().Get(request.getMessageId());
						handleRequst.stopTimer();
						handleRequst.setLifeTimeExpire(true);
						HandleRequestStore.getInstance().Remove(handleRequst.getMessageId());
						dbOperationHandler("DELETE",handleRequst);
						handleRequst.logPrint();
						Send(handleRequst);
					} else {
						logger.error("requested message Id is not exist " + request.getTitle());
					}
				} else {
					logger.error("Message Type Not Supported " + request.getTitle());
				}

			} catch (Exception e) {
				e.printStackTrace();
				channel.close();
				logger.error("failed to handle incoming message over SCTP connection " + e);
			}

		} else {
			channel.close();
			return;
		}
	}

	private HandleRequst createHandler(Request request) {

		HandleRequst handleRequst = null;
		try {
			handleRequst = new HandleRequst();
			handleRequst.setMessageTitle(request.getTitle());
			handleRequst.setMessageId(request.getMessageId());
			handleRequst.setLifeTime(request.getLifeTime());
			handleRequst.setIntervalTime(request.getInterval());
			handleRequst.setPayloadData(request.getPayload().getStrem().toByteArray());
			handleRequst.assignValues();
			// Store in Memory as well as in DB
			HandleRequestStore.getInstance().Add(handleRequst);
			dbOperationHandler("ADD",handleRequst);
			return handleRequst;
		} catch (Exception e) {
			logger.error("failed to create handleRequest insance " + e);
		}
		return handleRequst;
	}

	private class EventDeliveryHandler implements Runnable {

		private Event event;
		private String peer;

		public EventDeliveryHandler(Event event, String peer) {
			this.event = event;
			this.peer = peer;
		}

		public void run() {
			ThreadContext.put("Peer-FSM", peer);
			try {
				if (logger.isDebugEnabled()) {
					logger.debug(String.format("got event type=%s on peer=%s in processor thread",
							event.eventType.getType(), peer));
				}
				synchronized (this) {
					invokeFSM(this.event);
				}
			} catch (Exception e) {
				logger.error(
						String.format("failed to handle event type=%s on peer=%s", event.eventType.getType(), peer), e);

			} finally {
				ThreadContext.getContext().remove("Peer-FSM");
			}
		}
	}

	public Request instantiateMessageFromBinary(final byte[] binaryRequest) {

		Request request = null;
		try {
			InputStream targetStream = new ByteArrayInputStream(binaryRequest);
			Message message = null;
			try {
				message = Message.parseFrom(targetStream);
			} catch (IOException e) {
				e.printStackTrace();
			}
			request = message.getRequest();
		} catch (Exception ipbe) {
			logger.error("ERROR: Unable to instantiate Message instance from provided binary data - " + ipbe);
		}
		return request;

	}
	
	public void dbOperationHandler(String opType,HandleRequst handleRequst) {
		try {
			
			if(this.dbSupport) {
				if (opType.equalsIgnoreCase("ADD")) {
					try {
						SchdularStack.getRequestDBStore().Add(handleRequst);
					} catch (Exception e) {
						logger.error("failed to ADD requets in db " + e);
					}
				} else if (opType.equalsIgnoreCase("UPDATE")) {
                    try {
                    	SchdularStack.getRequestDBStore().Update(handleRequst);
					} catch (Exception e) {
						logger.error("failed to UPDATE requets in db " + e);
					}
				} else if (opType.equalsIgnoreCase("DELETE")) {
                    try {
                    	SchdularStack.getRequestDBStore().Delete(handleRequst);
					} catch (Exception e) {
						logger.error("failed to DELETE requets in db " + e);
					}
				} else {
					logger.error(String.format("ERROR: Unable to perform opertaion for optype=%s due to unsupported operationType" ,opType));
				}
			}
			
		}catch(Exception e) {
			logger.error(String.format("ERROR: Unable to perform optype=%s " ,opType), e);
		}
	}
	

	public com.comCode.ProtocolBuffer.Payload generatePayload(byte[] payloadData) {
		com.comCode.ProtocolBuffer.Payload.Builder builder = com.comCode.ProtocolBuffer.Payload.newBuilder();
		ByteString byteString = ByteString.copyFrom(payloadData);
		com.comCode.ProtocolBuffer.Payload payload = builder.setStrem(byteString).build();
		return payload;
	}

	public Request generateRequest(HandleRequst handleRequst) {
		com.comCode.ProtocolBuffer.Request.Builder builder = com.comCode.ProtocolBuffer.Request.newBuilder();
		Request request = builder.setTitle(handleRequst.getMessageTitle()).setInterval(handleRequst.getIntervalTime())
				.setLifeTime(handleRequst.getLifeTime()).setMessageId(handleRequst.getMessageId())
				.setPayload(generatePayload(handleRequst.getPayloadData())).build();
		return request;
	}

}
