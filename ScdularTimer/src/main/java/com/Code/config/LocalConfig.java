package com.Code.config;

import com.google.gson.annotations.Expose;

public class LocalConfig {

	@Expose
    protected String hostName;
	@Expose
	protected String ipAddress; 
	@Expose
    protected int listenerPort;
	
	public LocalConfig()    {

    }

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public int getListenerPort() {
		return listenerPort;
	}

	public void setListenerPort(int listenerPort) {
		this.listenerPort = listenerPort;
	}
	
	
}
