package com.Code.config;

public class SchdularConfig {

	private int totalNoTimer;
	private int expiretimer;
	private int factoryExpireTimer;
	private boolean dbSupport;

	public SchdularConfig() {

	}

	public boolean isDbSupport() {
		return dbSupport;
	}

	public void setDbSupport(boolean dbSupport) {
		this.dbSupport = dbSupport;
	}

	public int getTotalNoTimer() {
		return totalNoTimer;
	}

	public void setTotalNoTimer(int totalNoTimer) {
		this.totalNoTimer = totalNoTimer;
	}

	public int getExpiretimer() {
		return expiretimer;
	}

	public void setExpiretimer(int expiretimer) {
		this.expiretimer = expiretimer;
	}

	public int getFactoryExpireTimer() {
		return factoryExpireTimer;
	}

	public void setFactoryExpireTimer(int factoryExpireTimer) {
		this.factoryExpireTimer = factoryExpireTimer;
	}

}
