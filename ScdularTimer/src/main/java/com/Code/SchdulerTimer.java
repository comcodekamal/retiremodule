package com.Code;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import com.Code.Handler.HandleRequst;
import com.Code.SCTPReceiver.Event;
import com.Code.SCTPReceiver.EventType;

public class SchdulerTimer implements Runnable {
	private static Logger logger = Logger.getLogger(SchdulerTimer.class);
	private static ScheduledExecutorService timerService = Executors.newScheduledThreadPool(8);
	private HandleRequst handleRequst;
	private ScheduledFuture<?> timer;

	private SchdulerTimer(HandleRequst handleRequst) {
		this.handleRequst = handleRequst;
	}

	public static SchdulerTimer startTimer(HandleRequst handleRequst) {
		logger.info("Starting timer for MessageID " + handleRequst.getMessageId() + "   Start Time:: "
				+ handleRequst.getStartTime());
		SchdulerTimer timer = new SchdulerTimer(handleRequst);
		timer.timer = SchdulerTimer.timerService.schedule(timer, handleRequst.getIntervalTime(), TimeUnit.MILLISECONDS);
		return timer;
	}

	public void run() {
		try {
			this.handleRequst.setExpireTime(java.time.LocalDateTime.now().toString());
			logger.error(" expire interval timer for messagId start " + this.handleRequst.getMessageId());
			Event event = new Event(EventType.INTERVAL_TIMER_EXPIRY, this.handleRequst, null);
			SchdularStack.getSchularChannelRecevier().InvokeFSM(event);
		} catch (Exception e) {
			logger.error(String.format("ERROR: Unable to process timer for messageId[%d]", handleRequst.getMessageId()) + e);
		}
	}

	public void cancel(boolean mayInterruptIfRunning) {
		logger.error(String.format(" Stopping timer for messageId[%d] ", handleRequst.getMessageId()));
		this.timer.cancel(mayInterruptIfRunning);
	}
}
