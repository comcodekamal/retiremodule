package com.comCode.Store;

import java.util.concurrent.ConcurrentHashMap;
import com.Code.Handler.HandleRequst;

public class HandleRequestStore {
	
	protected static HandleRequestStore instance;

	protected ConcurrentHashMap<Integer, HandleRequst> handleRequsts;

	protected HandleRequestStore() {
		handleRequsts = new ConcurrentHashMap<Integer, HandleRequst>();
	}

	public static void Init() {
		HandleRequestStore.instance = new HandleRequestStore();
	}

	public static HandleRequestStore getInstance() {
		return instance;
	}

	public HandleRequst Get(int messageId) {
		return handleRequsts.get(messageId);
	}

	public void Add(HandleRequst handleRequst) {
		this.handleRequsts.put(handleRequst.getMessageId(), handleRequst);
	}

	public ConcurrentHashMap<Integer, HandleRequst> getHandleRequsts() {
		return handleRequsts;
	}

	public HandleRequst Remove(int messageId) {
		return handleRequsts.remove(messageId);
	}

}
