package com.comCode.storage.impl;

import org.apache.log4j.Logger;

import com.Code.SchdularStack;
import com.Code.SchdulerTimer;
import com.Code.Handler.HandleRequst;
import com.Code.SCTPReceiver.SchularChannelRecevier;

public class DbOperation implements Runnable {
	private static Logger logger = Logger.getLogger(DbOperation.class);
	private HandleRequst handleRequst;
	private String opertionType;
	private SchularChannelRecevier schularChannelRecevier;
	private SchdulerTimer schdulerTimer;

	public DbOperation(HandleRequst handleRequst,String opertionType, SchularChannelRecevier schularChannelRecevier) {
		this.handleRequst = handleRequst;
		this.opertionType=opertionType;
		this.schularChannelRecevier = schularChannelRecevier;
		storeDb();
	}

	public HandleRequst getHandleRequst() {
		return handleRequst;
	}

	private void storeDb() {
		try {
			SchdularStack.getRequestDBStore().Add(this.handleRequst);
		} catch (Exception e) {
			logger.error(String.format("ERROR: Unable to store in db") + e);
		}
	}

	public void run() {
		try {
			HandleRequst handleRequst = null;
			handleRequst = processRequest();
			this.schularChannelRecevier.Send(handleRequst);
		} catch (Exception e) {
			logger.error(String.format("ERROR: Unable to process") + e);
		}
	}

	private HandleRequst processRequest() {
		try {
            this.handleRequst.startTimer();
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(String.format("ERROR: Unable to process") + e);
		}
		return null;
	}

}
