package com.comCode.storage.impl;

import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import org.apache.log4j.Logger;
import com.Code.Handler.HandleRequst;
import com.comCode.HibernateFactory.HibernateUtil;
import com.comCode.storage.api.RequestDBStore;

public class PersistentRequestStoreImpl extends RequestDBStore {
	private static Logger logger = Logger.getLogger(PersistentRequestStoreImpl.class);

	public PersistentRequestStoreImpl() {

	}

	@Override
	public void Add(HandleRequst handleRequst) {

		/*--Now persist--*/
		this.add(handleRequst);
	}

	@Override
	public HandleRequst Get(int key) {

		HandleRequst handleRequst = null;
		/*--try DB--*/
		handleRequst = this.get(key);
		return handleRequst;
	}

	@Override
	public ArrayList<HandleRequst> Get() {
		try {
			ArrayList<HandleRequst> handleRequstList = null;
			/*--try DB--*/
			handleRequstList = this.getList();

			return handleRequstList;

		} catch (Exception e) {
			logger.error(String.format("failed to find handleRequstInfo"), e);
		}
		return null;
	}

	@Override
	public void Delete(HandleRequst handleRequst) {
		/*--now delete from persistent store--*/
		this.delete(handleRequst);
		return;
	}

	@Override
	public void Update(HandleRequst handleRequst) {

		/*--now update the persistent store--*/
		this.update(handleRequst);
		return;
	}

	private void add(HandleRequst handleRequst) {
		try {
			EntityManagerFactory entityManagerFactory = HibernateUtil.getInstance().getEntityManagerFactory();
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			EntityTransaction entityTransaction = entityManager.getTransaction();
			entityTransaction.begin();
			entityManager.persist(handleRequst);
			entityManager.getTransaction().commit();
			entityManager.close();
			if (logger.isInfoEnabled()) {
				logger.info(String.format("Added HandleRequstInfo=%s ", handleRequst.toString()));
			}

		} catch (Exception e) {
			logger.error(String.format("ERROR: Unable to persist handleRequest for messageId[%d] ",
					handleRequst.getMessageId()) + e);
		}
	}

	private HandleRequst get(int key) {
		try {
			HandleRequst handleRequst = null;
			EntityManagerFactory entityManagerFactory = HibernateUtil.getInstance().getEntityManagerFactory();
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			EntityTransaction entityTransaction = entityManager.getTransaction();
			entityTransaction.begin();
			handleRequst = entityManager.find(HandleRequst.class, key);
			entityManager.getTransaction().commit();
			entityManager.close();
			if (logger.isDebugEnabled()) {
				logger.debug(String.format("found handleRequstInfo=%s", handleRequst.toString()));
			}

			return handleRequst;
		} catch (Exception e) {
			logger.error(String.format("ERROR: Unable to find handleRequest for messageId[%d] from DB ", key) + e);
		}

		return null;
	}

	private void delete(HandleRequst handleRequst) {
		try {

			EntityManagerFactory entityManagerFactory = HibernateUtil.getInstance().getEntityManagerFactory();
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			EntityTransaction entityTransaction = entityManager.getTransaction();
			entityTransaction.begin();
			entityManager
					.remove(entityManager.contains(handleRequst) ? handleRequst : entityManager.merge(handleRequst));
			entityManager.getTransaction().commit();
			entityManager.close();
			if (logger.isDebugEnabled()) {
				logger.debug(String.format("deleted key=%d ", handleRequst.getMessageId()));
			}
		} catch (Exception e) {
			logger.error(String.format("ERROR: Unable to delete handleRequest for messageId[%d] ",
					handleRequst.getMessageId()) + e);
		}
	}

	private void update(HandleRequst handleRequst) {
		try {
			EntityManagerFactory entityManagerFactory = HibernateUtil.getInstance().getEntityManagerFactory();
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			EntityTransaction entityTransaction = entityManager.getTransaction();
			entityTransaction.begin();
			entityManager.merge(handleRequst);
			entityManager.getTransaction().commit();
			entityManager.close();
			if (logger.isDebugEnabled()) {
				logger.debug(String.format("Object Updated successfully.....!! key=%d ", handleRequst.getMessageId()));
			}
		} catch (Exception e) {
			logger.error(String.format("ERROR: Unable to update handleRequest for messageId[%d] from DB ",
					handleRequst.getMessageId()) + e);
		}
	}

	private ArrayList<HandleRequst> getList() {
		try {
			ArrayList<HandleRequst> handleRequstList = new ArrayList<HandleRequst>();
			EntityManagerFactory entityManagerFactory = HibernateUtil.getInstance().getEntityManagerFactory();
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			EntityTransaction entityTransaction = entityManager.getTransaction();
			entityTransaction.begin();
			handleRequstList = (ArrayList<HandleRequst>) entityManager
					.createQuery("FROM handleRequst p", HandleRequst.class).getResultList();
			entityManager.getTransaction().commit();
			entityManager.close();
			if (logger.isDebugEnabled()) {
				for (HandleRequst handleRequst : handleRequstList) {
					logger.debug(String.format("found handleRequstInfo=%s", handleRequst.toString()));
				}
			}
			return handleRequstList;

		} catch (Exception e) {
			logger.error(String.format("ERROR: Unable to get All handleRequest from DB ") + e);
		}
		return null;
	}

}
