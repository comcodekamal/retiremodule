package com.comCode.storage.api;

import java.util.ArrayList;
import com.Code.Handler.HandleRequst;

public abstract class RequestDBStore {

	public abstract void Add(HandleRequst handleRequst);

	public abstract HandleRequst Get(int key);

	public abstract ArrayList<HandleRequst> Get();

	public abstract void Delete(HandleRequst handleRequst);

	public abstract void Update(HandleRequst handleRequst);

}
