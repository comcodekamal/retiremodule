package com.comCode.HibernateFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.apache.log4j.Logger;

public class HibernateUtil {
	private static Logger logger = Logger.getLogger(HibernateUtil.class);
	protected static HibernateUtil instance;
	protected EntityManagerFactory entityManagerFactory;

	public HibernateUtil() {
		try {
			entityManagerFactory = createEntityManagerFactory();
		} catch (Exception e) {
			logger.error(String.format(" ERROR: Unable to instantiate Entity Manager Factory "),e);
		}
	}

	private EntityManagerFactory createEntityManagerFactory() {
		try {
			entityManagerFactory = Persistence.createEntityManagerFactory("myPu");
		} catch (Exception e) {
			logger.error(String.format("ERROR: Unable to load Entity Manager Factory configuration "),e);
		}
		return entityManagerFactory;
	}

	public static void Init() {
		instance = new HibernateUtil();
	}

	public static HibernateUtil getInstance() {
		return instance;
	}

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void closeEntityManagerFactory() {
		entityManagerFactory.close();
	}

}
